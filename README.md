Parallel Programming to Solve 2D Poisson's equation with Multigrid Methods

                          ./+o+-       liuyuhang@liaoning
                  yyyyy- -yyyyyy+      OS: Ubuntu 18.04 bionic
               ://+//////-yyyyyyo      Kernel: x86_64 Linux 4.15.0-42-generic
           .++ .:/++++++/-.+sss/`      Uptime: 21d 18h 55m
         .:++o:  /++++++++/:--:/-      Packages: 2160
        o:+o+:++.`..```.-/oo+++++/     Shell: bash 4.4.19
       .:+o:+o/.          `+sssoo+/    Resolution: 1440x900
  .++/+:+oo+o:`             /sssooo.   DE: Gnome 
 /+++//+:`oo+o               /::--:.   WM: GNOME Shell
 \+/+o+++`o++o               ++////.   WM Theme: Adwaita
  .++.o+++oo+:`             /dddhhh.   GTK Theme: Ambiance [GTK2/3]
       .+.o+oo:.          `oddhhhh+    Icon Theme: ubuntu-mono-dark
        \+.++o+o``-````.:ohdhhhhh+     Font: Ubuntu 11
         `:o+++ `ohhhhhhhhyo++os:      CPU: Intel Xeon Gold 5120 CPU @ 56x 3.2GHz
           .o:`.syhhhhhhh/.oo++o`      GPU: Quadro P400
               /osyyyyyyo++ooo+++/     RAM: 3833MiB / 128573MiB
                   ````` +oo+++o\:    
                          `oo++.      
liuyuhang@liaoning:~$ mpif90 -v
mpifort for MPICH version 3.2.1
ifort version 18.0.3
liuyuhang@liaoning:~$
